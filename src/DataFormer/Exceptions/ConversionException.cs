﻿using System;

namespace DataFormer.Exceptions {
    ///
    public class ConversionException : Exception {
        ///
        public long ConverterCode { get; set; } = 0;
        ///
        public string ConverterOutput { get; set; } = "";
        ///
        public string ConverterError { get; set; } = "";

        ///
        public ConversionException() : base() { }
        ///
        public ConversionException(string message) : base(message) { }
        ///
        public ConversionException(string message, Exception inner) : base(message, inner) { }

    }
}
