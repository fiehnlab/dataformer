﻿using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DataFormer.Api;
using DataFormer.Services;
using DataFormer.Models;
using DataFormer.Utility;
using DataFormer.Utility.Unzip;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.PlatformAbstractions;

namespace DataFormer {
    ///
    public class Startup {
        ///
        public IConfigurationRoot Configuration { get; }

        ///
        public Startup(IHostingEnvironment env) {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        ///
        public void ConfigureServices(IServiceCollection services) {
            // Add framework services.
            services.AddOptions();
            services.AddRouting();
            services.Configure<DataFormerConfig>(Configuration.GetSection("DataFormer"));

            services.AddMvc().AddJsonOptions(jsonOptions => {
                jsonOptions.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            });

            services.Configure<FormOptions>(x => {
                x.BufferBody = false;
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue; // In case of multipart
            });

            services.AddTransient<IUnzipper, Unzipper>();
            services.AddTransient<IConvertService, ConvertRunnerService>();

            // Add Swagger services
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info {
                    Version = "v1",
                    Title = "DataForner API",
                    Description = "A Raw data conversion utility",
                    TermsOfService = "None",
                    Contact = new Contact { Name = "Diego Pedrosa", Email = "dpedrosa@ucdavis.edu", Url = "" },
                    License = new License { Name = "Use under CC by 4.0", Url = "" }
                });

                // Set the comments path for the Swagger JSON and UI.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "DataFormer.xml");
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        ///
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            if (env.IsDevelopment() || env.IsEnvironment("Testing")) {
                loggerFactory.AddDebug(LogLevel.Debug);
                app.UseDeveloperExceptionPage();
            } else {
                loggerFactory.AddDebug(LogLevel.Information);
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "DataFormer API");
            });

            if (env.WebRootPath == null || env.WebRootPath.Equals("")) {
                env.WebRootPath = ConfigurationPath.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                Directory.CreateDirectory(env.WebRootPath);
            }

            app.UseStatusCodePages();
            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "rest/{controller=conversion}/{action=upload}/{file?}/{type?}");
            });
        }
    }
}
