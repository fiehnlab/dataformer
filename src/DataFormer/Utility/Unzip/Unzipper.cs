﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace DataFormer.Utility.Unzip {
    ///
    public class Unzipper : IUnzipper {
        private List<string> validFiles = new List<string>() { ".d", ".wiff" };

    /// <summary>
    /// Uncompresses the file 
    /// </summary>
    /// <param name="filename"></param>
    /// <param name="outPath"></param>
    /// <returns></returns>
    public string Unzip(string filename, string outPath) {
            var mainfile = "";
			if (Directory.Exists(outPath)) {
				Directory.EnumerateFileSystemEntries(outPath).ToList().ForEach(entry => DeleteEntry(entry));
			}

			if (File.Exists(filename)) {
                using(ZipArchive archive = ZipFile.OpenRead(filename)) {
                    archive.ExtractToDirectory(outPath);
                }

                mainfile = Directory.GetFileSystemEntries(outPath).Where(fileItem => {
                    Console.Out.WriteLine($"file item: {fileItem}");
                    return validFiles.Contains(new FileInfo(fileItem).Extension);
                }).First();

                File.Delete(filename);
			} else {
                throw (new Exception($"Invalid zipfile '{filename}'"));
            }

            return mainfile;
        }

		private void DeleteEntry(string entry) {
			if (File.GetAttributes(entry).HasFlag(FileAttributes.Directory)) {
				Directory.Delete(entry, true);
			} else {
				if (!entry.EndsWith(".zip")) {
					File.Delete(entry);
				}
			}
		}
    }
}
