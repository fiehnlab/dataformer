﻿using DataFormer.Api;
using DataFormer.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Linq;

namespace DataFormer.Controllers {
    ///
    [Route("rest/[controller]")]
    public class ConversionController : Controller {
        ///
        private static readonly string CONTENT_TYPE = "application/octet-stream";
        // ///
        // private static readonly string UPLOAD_BASE = "uploads";
        // ///
        // private static readonly string DOWNLOAD_BASE = "downloads";

        ///
        private readonly IHostingEnvironment env;
        ///
        private readonly ILogger<ConversionController> logger;
        ///
        private readonly IConvertService service;
        ///
        private readonly DataFormerConfig config;

        ///
        public ConversionController(IHostingEnvironment environment, ILogger<ConversionController> log, IConvertService convertService, IOptions<DataFormerConfig> config) {
            env = environment;
            logger = log;
            service = convertService;
            this.config = config.Value;
        }

        ///
        [HttpGet("/rest/conversion/upload")]
        public IActionResult Index() {
            return View("UploadForm");
        }

        ///
        [HttpPost("/rest/conversion/upload")]
        [Consumes("multipart/form-data")]
        public IActionResult Upload(IFormFile file) {
            if (file == null || file.Length <= 0) {
                return BadRequest(new { message = "File cannot be empty." });
            }

            var fileInfo = new FileInfo(file.FileName);
            logger.LogDebug("FILENAME: " + fileInfo.FullName);
            DirectoryInfo dest = new DirectoryInfo(Path.Combine(config.StorageFolder, fileInfo.Name.Replace(fileInfo.Extension, "")));
            dest.Create();

            var save = Path.Combine(dest.FullName, fileInfo.Name);

            try {
                ConversionResult result;
                using (FileStream fs = new FileStream(save, FileMode.Create)) {
                    file.CopyTo(fs);
                    fs.Flush();
                }

                //convert to both abf and mzx?ml
                result = service.Convert(new FileInfo(save), Path.Combine(config.StorageFolder, dest.Name));

                return Ok(result.ToJson());

            } catch (Exception ex) {
                logger.LogError($"Upload Error: {ex.Message}");
                return BadRequest(new { message = ex.Message, stackTrace = ex.StackTrace, filename = file.FileName });
            }
        }

        /// <summary>
        /// Downloads a result file for the given process id
        /// </summary>
        /// <param name="file">name of the file that was submitted for conversion</param>
        /// <param name="type">type of the conversion to be downloaded, can be: abf or mzxml</param>
        /// <returns>A byte array holding the results data or a 400 error in case of missing result file</returns>
        // GET: rest/conversion/download/{file}/{type}
        [HttpGet("/rest/conversion/download/{file}/{type}")]
        public IActionResult Download(string file, string type) {
            if (file.Equals("") || type.Equals("")) {
                return BadRequest(new { message = "Need the filename of a file and the conversion type you want to download." });
            }
            logger.LogInformation($"\nrequested download of {type} version of {file}\n");

            FileInfo downloader = new FileInfo(Directory.GetFiles(Path.Combine(config.StorageFolder, file)).Where(_ => _.ToLower().EndsWith($".{type.ToLower()}")).Single<string>());

            if (downloader.Exists) {
                try {
                    FileContentResult result = new FileContentResult(System.IO.File.ReadAllBytes(downloader.FullName), CONTENT_TYPE) {
                        FileDownloadName = file
                    };
                    HttpContext.Response.ContentType = CONTENT_TYPE;

                    logger.LogInformation($"Downloading results ({result.FileContents.Length} bytes)");
                    return result;
                } catch (IOException ex) {
                    logger.LogError($"Downloading failed ({ex.Message})");
                    return BadRequest(new { error = "Can't open file for download. " + ex.Message });
                }

            } else {
                var msg = $"Can't find a result file in {downloader.DirectoryName}";
                logger.LogInformation($"Downloading failed ({msg})");
                return BadRequest(new { error = msg });
            }
        }
    }
}
