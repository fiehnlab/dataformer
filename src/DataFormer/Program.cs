﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;

namespace DataFormer {
    ///
    public class Program
    {
        ///
		public static void Main(string[] args) {
			var config = new ConfigurationBuilder()
				.AddCommandLine(args)
				.AddEnvironmentVariables(prefix: "ASPNETCORE_")
				.Build();

            var host = new WebHostBuilder()
                .UseUrls("http://*:9090")
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseWebRoot(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot"))
                .UseIISIntegration()
                .UseStartup<Startup>()
                .UseConfiguration(config)
                .UseSetting("detailedErrors", "true")
                .Build();

            host.Run();
        }

        ///
        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
            app.UseMvcWithDefaultRoute();
        }
    }
}
