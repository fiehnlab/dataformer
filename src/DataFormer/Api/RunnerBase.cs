﻿using DataFormer.Exceptions;
using DataFormer.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;

namespace DataFormer.Api {
    ///
    public abstract class RunnerBase {
        ///
        protected ILogger logger;
        ///
        public RunnerConfig config { get; }

        ///
        public string Name { get => GetType().Name; }

        /// Returns the list of formats this runner converts to
        public IList<string> GetExtensions() => config.filetypes.SelectMany(_ => _.filetype) as List<string>;

        /// Returns the preffered formats this runner converts to
        public abstract string GetDefaultExtension();

        ///
        public RunnerBase(RunnerConfig conf, ILoggerFactory logFac) {
            logger = logFac.CreateLogger<RunnerBase>();
            config = conf;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file">raw file to be converted</param>
        /// <param name="type">type of the destination file</param>
        /// <param name="output">folder to save the converted file</param>
        /// <returns></returns>
        protected abstract ProcessStartInfo GetProcessStartInfo(FileInfo file, string type, string output);

        /// <summary>
        /// Runs a conversion creating an abf file
        /// </summary>
        /// <param name="file">raw file to be converted</param>
        /// <param name="output">folder to save converted data</param>
        /// <returns></returns>
        public string doConversion(FileInfo file, string outputType, string output) {
            //logger.LogInformation($"\tRunning {Name} with params: \n\tfile: {file.FullName} \n\toutputType: {outputType} \n\toutput: {output}");

            try {
                Directory.CreateDirectory(output);
            } catch (Exception ex) {
                logger.LogError($"Cannot create directory: {output}");
                throw new ConversionException ( $"Cannot create directory: {output}\n\t-- {ex.Message}") { ConverterCode = -2 };
            }

            ProcessStartInfo startInfo = GetProcessStartInfo(file, outputType, output);

            var stdout = "";
            var stderr = "";
            long code = -1;
            try {
                var response = "";
                Process conversion = new Process();

                conversion = Process.Start(startInfo);
                stdout = conversion.StandardOutput.ReadToEnd();
                stderr = conversion.StandardError.ReadToEnd();
                conversion.WaitForExit();

                if (conversion.ExitCode == 0) {
                    response = "ok";
                } else {
                    conversion.Dispose();
                    throw new ConversionException($"Can't convert file '{Name}'") { ConverterCode = conversion.ExitCode, ConverterOutput = stdout, ConverterError = stderr };
                }

                conversion.Dispose();
                return response;

            } catch (Exception ex) {
                string msg = $"Error: {ex.Message}\nStack: {ex.StackTrace}\nProcess out: {stdout}\nProcess err: {stderr}";
                logger.LogError(new EventId(), msg);

                if (ex.GetType().IsAssignableFrom(typeof(ConversionException))) {
                    throw (ex);
                } else if(ex.GetType().IsAssignableFrom(typeof(Win32Exception))) {
                    throw new ConversionException($"Converter {Name} cannot be found") { ConverterCode = code, ConverterOutput = stdout, ConverterError = stderr };
                } else { 
                    throw new ConversionException(ex.Message, ex) { ConverterCode = code, ConverterOutput = stdout, ConverterError = stderr };
                }
            }
        }
    }
}
