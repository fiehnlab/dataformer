﻿namespace DataFormer.Utility {
    ///
    public interface IUnzipper {
        ///
        string Unzip(string filename, string outPath);
    }
}
