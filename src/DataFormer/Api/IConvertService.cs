﻿using DataFormer.Models;
using System.IO;

namespace DataFormer.Api {
    ///
    public interface IConvertService {
        ///
        ConversionResult Convert(FileInfo filename, string output);
    }
}
