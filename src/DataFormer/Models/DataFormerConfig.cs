﻿using System.Linq;

namespace DataFormer.Models {
    /// Application config class
    public class DataFormerConfig {
        /// List of runners available
        public RunnerConfig[] runners { get; set; }
        /// Where are the files read from / written to
        public string StorageFolder { get; set; }
        /// Returns the configuration of a specific runner
        public RunnerConfig GetRunnerConfig(string name) {
            return runners.Single(it => it.name.Equals(name));
        }
        /// ToString() override
        public override string ToString() {
            return $"Number of converters: {runners.Length}\n\t{string.Join("\n\t", runners.Select(it => it.ToString()))}";
        }
    }

    /// Configuration class for a conversion Runner
    public class RunnerConfig {
        /// specifies if the runner should be used or not
        public bool enabled { get; set; }
        /// runner name
        public string name { get; set; }
        /// runner executable file
        public string executable { get; set; }
        /// runner home folder
        public string folder { get; set; }
        /// list of file types supported by this runner
        public FileType[] filetypes { get; set; }
        /// ToString() override
        public override string ToString() {
            return $"name: {name}, enabled: {enabled}, executable: {executable}, folder: {folder}, filetypes: {filetypes}";
        }
    }

    /// Config class for a file type definition
    public class FileType {
        /// file type to use for the conversion
        public string filetype { get; set; }
        /// commandline arguments to use
        public string runargs { get; set; }
        /// ToString() override
        public override string ToString() {
            return $"filetype: {filetype}, runargs: {runargs}";
        }
    }
}
