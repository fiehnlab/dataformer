﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DataFormer.Models {
    ///
    public class ConversionResult {

        ///
        public string Filename { get; private set; }
        private Dictionary<string, string> status = new Dictionary<string, string>();

        ///
        public ConversionResult(string filename) {
            Filename = filename;
        }

        ///
        public void AddStatus(string type, string data) => status.Add(type, data);
        ///
        public string GetStatus(string type) => status[type.ToLower()];
        ///
        public Dictionary<string, string> GetStatusList() => status;
        ///
        public override string ToString() => $"Filename: {Filename}, {GetStatusString()}";
        ///
        public string ToJson() {

            return $"{{\"filename\":\"{Filename}\",{GetStatusJson()}}}";
        }

        private string GetStatusJson() {
            var statStr = new StringBuilder();
            foreach (KeyValuePair<string, string> kv in status) {
                statStr.Append("\"").Append(kv.Key).Append("\":\"").Append(kv.Value).Append("\",");
            }

            return statStr.ToString().TrimEnd(',');
        }
        private string GetStatusString() {
            var statStr = new StringBuilder();
            foreach (KeyValuePair<string, string> kv in status.OrderBy(e => e.Key)) {
                statStr.Append(kv.Key).Append(": ").Append(kv.Value).Append(",");
            }

            return statStr.ToString().TrimEnd(',');
        }
    }
}
