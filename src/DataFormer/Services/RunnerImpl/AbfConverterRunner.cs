﻿using Microsoft.Extensions.Logging;
using DataFormer.Models;
using DataFormer.Api;
using System.Diagnostics;
using System.IO;

namespace DataFormer.Services {
    ///
    public class AbfConverterRunner : RunnerBase {
        ///
        public AbfConverterRunner(RunnerConfig conf, ILoggerFactory logFac) : base(conf, logFac) {}
        ///
        public override string GetDefaultExtension() => ".abf";
        ///
        protected override ProcessStartInfo GetProcessStartInfo(FileInfo file, string outputType, string output) => new ProcessStartInfo {
            CreateNoWindow = false,
            UseShellExecute = false,
            RedirectStandardError = true,
            RedirectStandardOutput = true,
            FileName = Path.Combine(config.folder, config.executable),
            WorkingDirectory = config.folder,
            Arguments = $"-i {file.FullName} -o {output}"
        };
    }
}
