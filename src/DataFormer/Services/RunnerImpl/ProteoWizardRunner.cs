﻿using System.Diagnostics;
using DataFormer.Api;
using DataFormer.Models;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Linq;

namespace DataFormer.Services {
    class ProteoWizardRunner : RunnerBase {
        public ProteoWizardRunner(RunnerConfig conf, ILoggerFactory logFac) : base(conf, logFac) { }

        public override string GetDefaultExtension() => "mzML";

        protected override ProcessStartInfo GetProcessStartInfo(FileInfo file, string outputType, string output) => new ProcessStartInfo {
            CreateNoWindow = false,
            UseShellExecute = false,
            RedirectStandardError = true,
            RedirectStandardOutput = true,
            FileName = Path.Combine(config.folder, config.executable),
            WorkingDirectory = config.folder,
            Arguments = $"{file.FullName} {config.filetypes.Single(_ => _.filetype.ToLower().Equals(outputType)).runargs} -o {output}"
        };
    }


}
