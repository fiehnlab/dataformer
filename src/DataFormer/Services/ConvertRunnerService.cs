﻿using DataFormer.Api;
using DataFormer.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.IO;
using System;
using DataFormer.Utility;
using System.Threading.Tasks;

namespace DataFormer.Services {
    ///
    public class ConvertRunnerService : IConvertService {
        private ILoggerFactory loggerFactory;
        private ILogger<ConvertRunnerService> logger;
        private IUnzipper unzipper;
        private DataFormerConfig config;
        private List<RunnerBase> runners = new List<RunnerBase>();

        ///
        public ConvertRunnerService(IOptions<DataFormerConfig> conf, ILoggerFactory loggerFactory, IUnzipper unzipper) {
            this.loggerFactory = loggerFactory;
            logger = loggerFactory.CreateLogger<ConvertRunnerService>();

            this.unzipper = unzipper;
            config = conf.Value;

            logger.LogCritical(config.GetRunnerConfig("ProteoWizardRunner").ToString());

            runners.Add(new AbfConverterRunner(config.GetRunnerConfig("AbfConverterRunner"), loggerFactory));
            runners.Add(new ProteoWizardRunner(config.GetRunnerConfig("ProteoWizardRunner"), loggerFactory));
        }

        ///
        public ConversionResult Convert(FileInfo filename, string output) {

            FileInfo fileToConvert;
            if (filename.Extension.Equals(".zip")) {
                var mainFile = unzipper.Unzip(filename.FullName, filename.DirectoryName);

                fileToConvert = new FileInfo(Path.Combine(filename.DirectoryName, mainFile));
            } else {
                fileToConvert = filename;
            }

            var results = new ConversionResult(filename.Name);

            Parallel.ForEach(runners, (runner) => {
                var status = new List<string>();
                if (runner.config.enabled) {
                    foreach (FileType ftype in runner.config.filetypes) {
                        var ext = ftype.filetype.ToLower().Replace(".", "");
                        try {
                            results.AddStatus(ext, runner.doConversion(fileToConvert, ext, output));
                        } catch (Exception ex) {
                            results.AddStatus(ext, ex.Message);
                        }
                    }
                } else {
                    results.AddStatus(runner.GetDefaultExtension().Replace(".", "").ToLower(), "disabled");
                }
            });
            return results;
        }
    }
}
