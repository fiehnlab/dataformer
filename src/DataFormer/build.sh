#!/bin/bash

if [ -n $1 ] ; then
  DEST="$1"
else
  DEST="./app"
fi

echo "dest: $DEST"

echo -e "\nRemoving old deployment..."
rm -rf $DEST

echo -e "\n\nUpdating dependencies... "
dotnet restore

echo -e "\n\nBuilding project..."
dotnet build

echo -e "\n\nDeploying to '$DEST' folder..."
dotnet publish -c Release -o $DEST -r win7-x64

echo -e "\n\n...done.\n"
