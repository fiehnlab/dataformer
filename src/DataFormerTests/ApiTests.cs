﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Xunit.Abstractions;
using DataFormer;
using DataFormer.Services;
using DataFormer.Api;
using DataFormer.Utility;
using DataFormer.Utility.Unzip;
using DataFormer.Models;
using Microsoft.Extensions.Configuration;

namespace DataFormerTests {
    public abstract class ApiTests {
        protected readonly ITestOutputHelper output;

        protected IWebHostBuilder webHostBuilder;
        protected LoggerFactory logFac = new LoggerFactory();
        protected string webRoot = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
        protected IConvertService conversionService;
        protected IUnzipper unzipper;

        protected IConfigurationRoot Config { get; }

        public ApiTests(ITestOutputHelper output) {
            this.output = output;

            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            Config = builder.Build();

            webHostBuilder = new WebHostBuilder().UseWebRoot(webRoot).UseEnvironment("Testing").UseStartup<Startup>();
            webHostBuilder.Configure(app => {
                app.UseMvc();
            })
            .ConfigureServices(services => {
                services.AddLogging();
                services.AddTransient<IConvertService, ConvertRunnerService>();
                services.AddTransient<IUnzipper, Unzipper>();
                services.Configure<DataFormerConfig>(Config.GetSection("DataFormer"));
                services.AddMvc();
            })
            .ConfigureLogging(loggerFactory => {
                loggerFactory.AddDebug();
                loggerFactory.AddConsole();
            });
        }

        protected static MultipartFormDataContent MultipartFormDataContent(string testFile) {
            var multiPartContent = new MultipartFormDataContent("boundary=---I+Hate_this=Boundary:thing");

            var fileStream = new FileStream(testFile, FileMode.Open, FileAccess.Read);
            var streamContent = new StreamContent(fileStream);
            streamContent.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");

            // Add the file key/value
            multiPartContent.Add(streamContent, "file", testFile.Substring(testFile.LastIndexOf("/") + 1));

            return multiPartContent;
        }


        protected string GetPath(string url) {
            return url.Replace("http://localhost/", "");
        }
    }
}
