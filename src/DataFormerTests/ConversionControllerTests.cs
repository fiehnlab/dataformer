using Microsoft.AspNetCore.TestHost;
using System;
using System.IO;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace DataFormerTests {
    public class ConversionControllerTests : ApiTests {
        public ConversionControllerTests(ITestOutputHelper output) : base(output) { }
        private string source = "j:\\p20repo";

        [Fact]
        public async Task TestUploadAsync() {
            var filename = "testA.d.zip";

            Assert.True(new FileInfo(Path.Combine(source,filename)).Exists);

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    var StorageFolder = "j:\\storage";
                    var response = await client.PostAsync("rest/conversion/upload", MultipartFormDataContent(Path.Combine(source, filename)));
                    var body = response.Content.ReadAsStringAsync().Result;

                    try {
                        response.EnsureSuccessStatusCode();
                    } catch (Exception ex) {
                        output.WriteLine("ERROR: " + body + "\nStack: " + ex.Message);
                        Assert.True(false);
                    }

                    Assert.NotEmpty(body);
                    Assert.Contains("\"filename\":\"testA.d.zip\"", body);
                    Assert.Contains("\"abf\":\"disabled\"", body);
                    Assert.Contains("\"mzml\":\"ok\"", body);
                    //Assert.Contains("\"mzxml\":\"ok\"", body);

                    //var abf = new FileInfo(Path.Combine(StorageFolder, filename, filename.Replace(".d.zip", ".abf")));
                    //Assert.True(abf.Exists);

                    var mzml = new FileInfo(Path.Combine(StorageFolder, filename.Replace(".zip",""), filename.Replace(".d.zip", ".mzml")));
                    Assert.True(mzml.Exists);

                    //var mzxml = new FileInfo(Path.Combine(StorageFolder, filename, filename.Replace(".d.zip", ".mzxml")));
                    //Assert.True(mzml.Exists);
                }
            }
        }
    }
}
