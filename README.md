# DataFormer #

This is a REST Api to automate the conversion of raw data files (Agilent .d, Sciex .wiff) to both .abf and .mzML formats.

### How do I get set up? ###

* Download and install .NET Core 1.1 or 2.0 SDK
* Clone this repository
* On the root folder (where the DataFormer.snl file resides) run

	```bash
	dotnet restore
	```

* After the dependencies have been installed you can open the project on Visual Studio or
* Publish a production version with:

	```bash
	dotnet build
	dotnet publish -c Release -r win10-x64 -o ./app
	```

* Move the 'app' folder to its final destination, then run 

	```bash
	DataFormer.exe
	```
	or 
	```bash
	dotnet DataFormer
	```

### How to use ###

There are two main endpoints to interact with the api:

* Upload and Convert a raw data file by POSTing the raw data file in a multipart/form-data body with 'file' as variable name to:

	_(**NOTE:** '.d' and '.wiff' files need to be zipped before submission.)_

	http://phobos.fiehnlab.ucdavis.edu:9090/rest/conversion/upload

	Example to convert __rawData.d__ to both _abf_ and _mzml_

	```bash
	curl -F "file=@rawData.d.zip" http://phobos.fiehnlab.ucdavis.edu:9090/rest/conversion/upload
	```

* Download a converted file from:

	http://phobos.fiehnlab.ucdavis.edu:9090/rest/conversion/download/{filename}/{type}

	where \<filename\> is the name of the uploaded file without the '.zip' extension; and \<type\> is one of _abf_ or _mzml_

	Example to download the mzml version of __rawData.d__:

	```bash
	curl http://phobos.fehnlab.ucdavis.edu:9090/rest/conversion/download/rawData.d/mzml
	```

### Who do I talk to? ###

* [Diego Pedrosa][1] (Lead Programer)
* [Gert Wohlgemuth][2] (Software Development Manager)

[1]: mailto:dpedrosa@ucdavis.edu
[2]: mailto:wohlgemuth@ucdavis.edu